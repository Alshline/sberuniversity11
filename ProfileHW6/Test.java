package ProfileHW6;

import java.util.List;

public class Test {
    UserDao userDao = new UserDao();
    BookDao bookDao = new BookDao();

    @org.junit.Test
    public void testSaveUser() throws Exception {

        System.out.println(getUserBooksByEmail("email"));
    }

    public List<Book> getUserBooksByEmail(String email) {
        User user = userDao.getUserByEmail(email);
        List<Book> books = user.getBooksList();
        return books;
    }
}
