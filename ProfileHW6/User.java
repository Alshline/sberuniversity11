package ProfileHW6;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Users")
@NamedQuery(name = "User.getAll", query = "SELECT c from User c")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Getter
    @Setter
    @Column(name = "firstname", nullable = false, length = 100)
    private String firstName;

    @Getter
    @Setter
    @Column(name = "lastname", nullable = false, length = 100)
    private String lastName;

    @Getter
    @Setter
    @Column(name = "dateofbirth", nullable = false, length = 100)
    private String dateOfBirth;

    @Getter
    @Setter
    @Column(name = "email", nullable = false, length = 100, unique = true)
    private String email;

    @Getter
    @Setter
    @Column(name = "phonenumber", nullable = false, length = 100, unique = true)
    private String phoneNumber;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Book.class, cascade = CascadeType.ALL)
    private List<Book> books = new ArrayList<>();

    public void addBook(Book book){
        books.add(book);
    }
    public List<Book> getBooksList(){
        return books;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", books=" + books +
                '}';
    }
}
