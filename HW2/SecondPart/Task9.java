package HW2.SecondPart;

import java.util.Scanner;

/**
 * На вход подается число N. Необходимо вывести цифры числа слева направо.
 * Решить задачу нужно через рекурсию.
 */

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите число");
        int n = input.nextInt();
        forwardOutput(n);
    }

    public static void forwardOutput(int x) {
        if (x == 0) {
            return;
        } else {
            forwardOutput(x / 10);
            System.out.print(x - (x / 10) * 10 + " ");
        }
    }
}
