package HW2.SecondPart;

import java.util.Scanner;

/**
 * На вход подается число N — количество строк и столбцов матрицы. Затем в
 * последующих двух строках подаются координаты X (номер столбца) и Y (номер
 * строки) точек, которые задают прямоугольник.
 * Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
 * заполненной нулями (см. пример) и вывести всю матрицу на экран.
 */

public class Task2 {
    public static void main(String[] args) {
        //Прием вводных данных
        Scanner input = new Scanner(System.in);
        System.out.println("Введите количество столбцов/строк");
        int n = input.nextInt();

        //Построение двумерного массива с полем
        int[][] table = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                table[i][j] = 0;
            }
        }

        // Получение координат (1 - столбец, 2 - строка)
        int[] firstCoordinate = new int[2];
        int[] secondCoordinate = new int[2];
        System.out.println("Введите первую координату");
        for (int i = 0; i < firstCoordinate.length; i++) {
            firstCoordinate[i] = input.nextInt();
        }
        System.out.println("Введите вторую координату");
        for (int i = 0; i < secondCoordinate.length; i++) {
            secondCoordinate[i] = input.nextInt();
        }
        //Определение верхней координаты и нижней
        int[] topCoordinate;
        int[] bottomCoordinate;
        if (firstCoordinate[1] < secondCoordinate[1]) {
            topCoordinate = firstCoordinate.clone();
            bottomCoordinate = secondCoordinate.clone();
        } else {
            topCoordinate = secondCoordinate.clone();
            bottomCoordinate = firstCoordinate.clone();
        }

        // Перестроение поля в зависимости от координат
        for (int i = topCoordinate[1]; i <= bottomCoordinate[1]; i++) {
            for (int j = topCoordinate[0]; j <= bottomCoordinate[0]; j++) {
                table[i][j] = 1;
            }
        }

        // Перестроение для получения полой части
        for (int i = topCoordinate[1] + 1; i < bottomCoordinate[1]; i++) {
            for (int j = topCoordinate[0] + 1; j < bottomCoordinate[0]; j++) {
                table[i][j] = 0;
            }
        }

        // Вывод поля в консоль
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}
