package HW2.FirstPart;

import java.util.Scanner;

/**
 * Создать программу генерирующую пароль.
 * На вход подается число N — длина желаемого пароля. Необходимо проверить,
 * что N >= 8, иначе вывести на экран "Пароль с N количеством символов
 * небезопасен" (подставить вместо N число) и предложить пользователю еще раз
 * ввести число N.
 * Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
 * вывести его на экран. В пароле должны быть:
 * ● заглавные латинские символы
 * ● строчные латинские символы
 * ● числа
 * ● специальные знаки(_*-)
 */

public class SpecTask1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите длину пароля");
        int n = input.nextInt();

        if (n < 8){
            System.out.println("Пароль с " +n+ " количеством символов небезопасен");
            System.out.println("Предлагаем следующий пароль -" + generatePassword());
        } else {
            System.out.println(generatePassword());
        }
    }

    public static boolean checkPassword(String password){
        if (password.matches("([a-zA-Z0-9\\_\\*\\-]{8,})")){
            return true;
        } else {
            return  false;
        }
    }

    public static String generatePassword(){

        char[] letters = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                          'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                          '1','2','3','4','5','6','7','8','9','0'}; //62
        char[] symbols = {'*','_','-'}; //3
        int length = 8;

        String resultString = null;
        while (true){
            int number;
            char pickedSymbol;
            char[] resultCharArray = new char[8];
            for (int i = 0; i < 8; i++){
                number = (int) (Math.random()*63);
                resultCharArray[i] = letters[number];
            }
            int countOfSymbols = (int) (Math.random()*4)+1;
            for (int j = 0; j <= countOfSymbols; j++){
                pickedSymbol = symbols[(int) (Math.random()*3)];
                resultCharArray[(int) (Math.random()*8)] = pickedSymbol;
            }
            String resultStringBeforeCheck = String.valueOf(resultCharArray);
            if (checkPassword(resultStringBeforeCheck)){
                resultString = resultStringBeforeCheck;
                break;
            }
        }

        return resultString;
    }
}
