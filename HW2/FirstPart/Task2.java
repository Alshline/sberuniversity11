package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого аналогично передается второй
 * массив (aj) длины M.
 * Необходимо вывести на экран true, если два массива одинаковы (то есть
 * содержат одинаковое количество элементов и для каждого i == j элемент ai ==
 * aj). Иначе вывести false.
 */

public class Task2 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        // Первый массив
        System.out.println("Введите длину первого массива");
        int n = input.nextInt();
        int[] ai = new int[n];
        System.out.println("Введите первый массив");
        for (int i = 0; i < n; i++){
            ai[i] = input.nextInt();
        }

        // Второй массив
        System.out.println("Введите длину второго массива");
        int m = input.nextInt();
        int[] aj = new int[m];
        System.out.println("Введите второй массив");
        for (int i = 0; i < n; i++){
            aj[i] = input.nextInt();
        }

        //Sort arrays
        //Sort first array with selection
        for (int i = 0; i < ai.length; i++){
            int index_i = i;

            for (int j = i+1; j < ai.length; j++){
                if (ai[index_i] > ai[j]){
                    index_i = j;
                }
            }
            if (i != index_i){
                int tempNumber = ai[i];
                ai[i] = ai[index_i];
                ai[index_i] = tempNumber;
            }
        }

        //Sort second array with bubble
        for (int i = aj.length-1; i > 0; i--){
            for (int j = 0; j < i; j++){
                if (aj[j] > aj[j+1]){
                    int tempNumber = aj[j];
                    aj[j] = aj[j+1];
                    aj[j+1] = tempNumber;
                }
            }
        }

        for (int i = 0; i < ai.length; i++){
            System.out.print(ai[i]);
        }
        System.out.println("");
        for (int i = 0; i < aj.length; i++){
            System.out.print(aj[i]);
        }

        // Check length
        boolean checkLength = (n == m) && (aj.length == ai.length);
        boolean checkArray = true;
        for (int l = 0; l < ai.length; l++) {
            if (ai[l] != aj[l]) {
                checkArray = false;
                break;
            }
        }

        boolean result = checkLength && checkArray;
        System.out.println(result);
    }
}

// Возможно было бы проще перебирать оба массива сразу без сортировок, но не могу понять,
// как быть с повторяющимися элементами
