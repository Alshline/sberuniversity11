package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо создать массив, полученный из исходного возведением в квадрат
 * каждого элемента, упорядочить элементы по возрастанию и вывести их на
 * экран.
 */

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int n = input.nextInt();
        int[] ai = new int[n];
        System.out.println("Введите массив");
        for (int i = 0; i < n; i++){
            ai[i] = input.nextInt();
        }
        // Get new array
        int[] bufferedArray = new int[n];
        for (int i = 0; i < ai.length; i++){
            bufferedArray[i] = ai[i]*ai[i];
        }
        //Sort this array
        for (int i = bufferedArray.length-1; i > 0; i--){
            for (int j = 0; j < i; j++){
                if (bufferedArray[j] > bufferedArray[j+1]){
                    int tempNumber = bufferedArray[j];
                    bufferedArray[j] = bufferedArray[j+1];
                    bufferedArray[j+1] = tempNumber;
                }
            }
        }
        //Print this array
        for (int j = 0; j < bufferedArray.length; j++){
            System.out.print(bufferedArray[j] + " ");
        }

    }
}
