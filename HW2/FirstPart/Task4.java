package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо вывести на экран построчно сколько встретилось различных
 * элементов. Каждая строка должна содержать количество элементов и сам
 * элемент через пробел.
 */

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int n = input.nextInt();
        int[] ai = new int[n];
        System.out.println("Введите массив");
        for (int i = 0; i < n; i++){
            ai[i] = input.nextInt();
        }

        //Detect of overlapped elements
        int count = 1;
        for (int i = 1; i < ai.length; i++){
            if (ai[i] == ai[i-1]){
                count++;
            } else {
                System.out.println(count + " " + ai[i-1]);
                count = 1;
            }
            if (i == ai.length-1){
                System.out.println(count + " " + ai[i]);
            }
        }
    }
}
