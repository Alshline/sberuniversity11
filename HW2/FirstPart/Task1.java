package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * вещественных чисел (ai) из N элементов.
 * Необходимо реализовать метод, который принимает на вход полученный
 * массив и возвращает среднее арифметическое всех чисел массива.
 * Вывести среднее арифметическое на экран.
 */

public class Task1 {

    public static void main(String[] args) {
        System.out.println("Введите длину массива");
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        System.out.println("Введите массив");

        // Создание массива
        double[] ai = new double[n];
        // Переменная суммы ячеек массива
        double sum = 0;
        // Буфер
        double buffer;
        // Сумма из массива
        double arraySum = 0;

        // Два метода - с массивом и без него (второй случай проще и быстрее)
        for (int i = 0; i < n; i++){
            buffer = input.nextDouble();
            sum += buffer;
            ai[i] = buffer;
        }

        for (int j = 0; j < ai.length; j++){
            arraySum += ai[j];
        }

        double result_1 = sum/n;
        double result_2 = arraySum/n;

        System.out.println(result_1);
        System.out.println(result_2);
    }
}
