package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается строка S, состоящая только из русских заглавных
 * букв (без Ё).
 * Java 11 Базовый модуль Неделя 5
 * ДЗ 2 Часть 1
 * Необходимо реализовать метод, который кодирует переданную строку с
 * помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв
 * нужно пробелом.
 */

public class Task6 {
    public static void main(String[] args) {

        String[] morzeArray = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..",
                "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-",
                "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        Scanner input = new Scanner(System.in);
        System.out.println("Введите строку");
        String s = input.next();

        char[] sToCharArray = s.toCharArray();
        String[] bufferedCharArray = new String[s.length()];


        for (int i = 0; i < s.length(); i++){
            int charCode = (int) sToCharArray[i];
            bufferedCharArray[i] = morzeArray[charCode-1040];
        }

        for (int j = 0; j < bufferedCharArray.length; j++){
            System.out.print(bufferedCharArray[j] + " ");
        }
    }
}
