package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого передается число M.
 * Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
 * для которого |ai - M| минимальное). Если их несколько, то вывести
 * максимальное число.
 */

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int n = input.nextInt();
        int[] ai = new int[n];
        System.out.println("Введите массив");
        for (int i = 0; i < n; i++){
            ai[i] = input.nextInt();
        }
        System.out.println("Введите число для поиска его ближайшего соседа");
        int m = input.nextInt();

        int indexOfArray = 0;
        int result = Math.abs(m - ai[0]);
        int bufferedResult;
        for (int i = 1; i < ai.length; i++){
            bufferedResult = Math.abs(m - ai[i]);
            if ((bufferedResult <= result) && (ai[i] > ai[indexOfArray])){
                indexOfArray = i;
                result = bufferedResult;
            }
        }
        System.out.println(ai[indexOfArray]);
    }
}
