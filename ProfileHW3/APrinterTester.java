package ProfileHW3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class APrinterTester<T extends APrinter> {
    T t;

    public void setterT(T t) {
        this.t = t;
    }

    public void invokePrinter(){
        if (t != null){
            try {
                Method invokedMethod = t.getClass().getMethod("print", int.class);
                invokedMethod.invoke(t,10);
            }
            /*catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException exception){
                exception.printStackTrace();
            }*/
            catch (NoSuchMethodException exception){
                exception.printStackTrace();
            } catch (IllegalAccessException exception){
                exception.printStackTrace();
            } catch (InvocationTargetException exception){
                exception.printStackTrace();
            }
        } else System.out.println("no instance");
    }

    public static void main(String[] args) {
        APrinter aPrinter = new APrinter();
        APrinterTester aPrinterTester = new APrinterTester();
        aPrinterTester.setterT(aPrinter);
        aPrinterTester.invokePrinter();
    }

}
