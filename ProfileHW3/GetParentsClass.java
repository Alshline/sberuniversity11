package ProfileHW3;

import ProfileHW3.ForTests.CClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetParentsClass {
    List<Class> arrayList = new ArrayList<>();

    public static void main(String[] args) {

        CClass cClass = new CClass();
        GetParentsClass getParentsClass = new GetParentsClass();
        getParentsClass.getParents(cClass);


    }

    public <T> void getParents(T t) {
        Class cls = t.getClass();

        //Class[] classesArray = cls.getClasses();
        List<String> interfacesList = new ArrayList<>();
        List<String> classesList = new ArrayList<>();
        while (cls != Object.class) {
            classesList.add(cls.getName());
            Class[] interfacesArray = cls.getInterfaces();
            String str = Arrays.asList(interfacesArray).toString();
            interfacesList.add(str);
            cls = cls.getSuperclass();
        }
        System.out.println(classesList);
        System.out.println(interfacesList);

    }
}