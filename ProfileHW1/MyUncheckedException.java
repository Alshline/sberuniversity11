package ProfileHW1;

public class MyUncheckedException extends ArrayIndexOutOfBoundsException {
    public MyUncheckedException(String message) {
        super(message);
    }
}
