package ProfileHW1;

import java.util.Scanner;

public class LegacyClass {
    public static void main(String[] args) throws Exception {
        int n = inputN();
        System.out.println("Успешный ввод!");
    }
    // Вероятно можно было бы вывести метод в отдельный класс исключение как я делал ранее, но для одного метода, думаю достаточно
    private static int inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (!(n < 100 && n > 0)) {
            throw new Exception("Неверный ввод");
        }
        return n;
    }

}
