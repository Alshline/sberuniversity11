package ProfileHW1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormValidator {

    public void checkName(String str) throws Exception {
        if (str.length() < 2 || str.length() > 20) {
            throw new Exception("check Name failed");
        }
    }

    public void checkBirthDate(String str) throws Exception {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/uuuu");
        LocalDate newLocalDate = LocalDate.parse(str, dateTimeFormatter);
        LocalDate oldLocalDate = LocalDate.parse("01.01.1900", dateTimeFormatter);
        if (oldLocalDate.isBefore(newLocalDate)) throw new Exception("U cant be older than 1900");
    }

    public void checkGender(String str) throws Exception {
        enum Gender {
            Male,
            Female
        }
        boolean result = false;
        for (Gender gender : Gender.values()) {
            if (str.equals(Gender.values().toString())) {
                result = true;
                break;
            }
        }
        if (!result) throw new Exception("Wrong gender");
    }

    //не совсем ясно, что означает "должен нормально конвертироваться в double", попробую обработать исключение внутри другого
    public void checkHeight(String str) throws Exception {
        try {
            double value = Double.parseDouble(str);
            if (value < 0) throw new Exception("Wrong Height");
        } catch (NumberFormatException numberFormatException) {
            System.out.println("Wrong number");
        }
    }
}
