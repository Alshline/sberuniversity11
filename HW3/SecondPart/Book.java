package HW3.SecondPart;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Book {
    private String bookName;
    private String authorName;
    private List<Integer> estimationList;

    public Book(String bookName, String authorName) {
        this.authorName = authorName;
        this.bookName = bookName;
        estimationList = new ArrayList<>();
    }

    public String getBookName() {
        return bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void rateBook(int number) {
        estimationList.add(number);
    }

    public List<Integer> getEstimationList() {
        return estimationList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return bookName.equals(book.bookName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookName);
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", authorName='" + authorName + '\'' +
                '}';
    }
}
