package HW3.SecondPart;

import java.util.ArrayList;
import java.util.List;

public class ClassForTests {
    public static void main(String[] args) {
        Visitor Andrew = new Visitor("Andrew");
        Visitor Pavel = new Visitor("Pavel");
        Visitor Sasha = new Visitor("Sasha");

        Library library = new Library();

        Book firstBook = new Book("first", "firstAuthor");
        Book secondBook = new Book("second", "secondAuthor");
        Book thirdBook = new Book("third", "secondAuthor");
        Book againFirstBook = new Book("first", "againFirst");

        library.addBook(firstBook);
        library.addBook(secondBook);
        library.addBook(thirdBook);

        List<Book> mergeByAuthor = new ArrayList<>();
        mergeByAuthor = library.getListByAuthor("secondAuthor");

        // 1 test
        System.out.println("1 test");
        library.addBook(thirdBook);

        library.removeBook(thirdBook);

        // 2 test
        System.out.println("2 test");
        library.removeBook(thirdBook);

        // 3 test
        System.out.println("3 test");
        library.addBook(againFirstBook);

        library.giveBookToVisitor(firstBook, Andrew);
        library.giveBookToVisitor(secondBook, Andrew);

        // 4 test
        System.out.println("4 test");
        library.giveBookToVisitor(thirdBook, Pavel);

        // 5 test
        System.out.println("5 test");
        library.giveBookToVisitor(firstBook, Pavel);

        library.returnBookFromVisitor(firstBook,Andrew,5);
        library.returnBookFromVisitor(secondBook,Andrew,4);

        // 6 test
        System.out.println("6 test");
        library.returnBookFromVisitor(firstBook,Andrew,3);

        // 7 test
        System.out.println("7 test");
        library.getListByAuthor("abraCadabra");
    }
}
