package HW3.SecondPart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Estimation {
    private Map<Book,Integer> averageEstimation;

    public Estimation(){
        averageEstimation = new HashMap<>();
        System.out.println("Added estimation");
    }

    public void estimateBook(Book book, int rating){
        book.rateBook(rating);
        List<Integer> estimationList = book.getEstimationList();
        int sum=0;
        for (int i = 0; i < estimationList.size(); i++){
            sum += estimationList.get(i);
        }
        averageEstimation.put(book,sum/ estimationList.size());
    }
}
