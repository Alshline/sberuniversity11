package HW3.SecondPart;

import java.util.ArrayList;
import java.util.List;

public class Visitor {
    private final String visitorName;
    private Integer id = null;
    private List<Book> visitorBooksList;

    public Visitor(String visitorName) {
        this.visitorName = visitorName;
        visitorBooksList = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public void addBook(Book book) {
        if (checkBooksList(book)){
            visitorBooksList.add(book);
        }
    }

    public boolean checkBooksList(Book book) {
        if (visitorBooksList.contains(book)) {
            System.out.println(visitorName + " have " + book);
            return true;
        } else {
            System.out.println(visitorName + " dont have " + book);
            return false;
        }
    }

    public List<Book> getVisitorBooksList() {
        return visitorBooksList;
    }
    public void removeBook (Book book){
        if (visitorBooksList.contains(book)){
            System.out.println(book + "removed from visitor list.");
            visitorBooksList.remove(book);
        }
    }

    @Override
    public String toString() {
        return visitorName;
    }
}
