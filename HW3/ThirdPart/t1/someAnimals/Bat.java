package HW3.ThirdPart.t1.someAnimals;

import HW3.ThirdPart.t1.BirdWayOfBirth;
import HW3.ThirdPart.t1.FlyingAnimals;

public class Bat extends FlyingAnimals implements BirdWayOfBirth {
    @Override
    public void flying() {
        System.out.println("flying slowly");
    }
}
