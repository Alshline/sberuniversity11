package HW3.ThirdPart.t1.someAnimals;

public class TEST {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Dolphin dolphin = new Dolphin();
        Eagle eagle = new Eagle();
        GoldFish goldFish = new GoldFish();

        bat.flying();
        bat.birth();
        bat.eat();
        bat.sleep();

        dolphin.swimming();
        dolphin.birth();
        dolphin.eat();
        dolphin.sleep();

        eagle.flying();
        eagle.birth();
        eagle.eat();
        eagle.sleep();

        goldFish.swimming();
        goldFish.birth();
        goldFish.eat();
        goldFish.sleep();
    }
}
