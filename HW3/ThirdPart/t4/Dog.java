package HW3.ThirdPart.t4;

public class Dog {
    private final String name;

    public Dog(String name) {
        this.name = name;
        System.out.println("Собака с кличкой " + name);
    }

    @Override
    public String toString() {
        return name;
    }
}
