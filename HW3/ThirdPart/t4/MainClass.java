/**
 * Переписать задачу 7 домашнего задания 2 часть 2 в стиле ООП.
 * a. Нужно как минимум завести классы Dog и Participant.
 * b. Нужно реализовать метод, определяющий трех победителей согласно
 * условию.
 * c. Можно добавлять любые дополнительные методы и классы.
 * Условие задачи:
 * Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
 * хранения участников и оценок неудобная, а победителя определить надо. В
 * первой таблице в системе хранятся имена хозяев, во второй - клички животных,
 * в третьей — оценки трех судей за выступление каждой собаки. Таблицы
 * связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
 * строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
 * помочь Пете определить топ 3 победителей конкурса.
 * На вход подается число N — количество участников конкурса. Затем в N
 * строках переданы имена хозяев. После этого в N строках переданы клички
 * собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
 * оценки судей. Победителями являются три участника, набравшие
 * максимальное среднее арифметическое по оценкам 3 судей. Необходимо
 * вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
 * Гарантируется, что среднее арифметическое для всех участников будет
 * различным.
 */

package HW3.ThirdPart.t4;

class MainClass {
    public static void main(String[] args) {
        System.out.println("Cоздаем участников");
        Judges judges = new Judges();

        Participant ivan = new Participant("Иван");
        Dog juchka = new Dog("Жучка");
        ivan.setDog(juchka);
        Participant nikolay = new Participant(new Dog("Кнопка"), "Николай");
        Participant anna = new Participant(new Dog("Цезарь"), "Анна");
        Participant darya = new Participant(new Dog("Добряш"), "Дарья");

        judges.addParticipant(ivan).addParticipant(nikolay).addParticipant(anna).addParticipant(darya);

        ivan.addRate(7, 6, 7);
        System.out.println(ivan.getAverageRate());

        nikolay.addRate(8, 8, 7);

        System.out.println(nikolay.getAverageRate());

        anna.addRate(4, 5, 6);
        System.out.println(anna.getAverageRate());

        darya.addRate(9, 9, 9);
        System.out.println(darya.getAverageRate());

        System.out.println(judges.getWinnersList());
    }
}
