/**
 * Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
 * сожалению, из Мебели он умеет чинить только Табуретки, а Столы, например,
 * нет. Реализовать метод в цеху, позволяющий по переданной мебели
 * определять, сможет ли ей починить или нет. Возвращать результат типа
 * boolean. Протестировать метод.
 */

package HW3.ThirdPart.t2;

class MainClass {
    public static void main(String[] args) {
        BestCarpenter bestCarpenter = new BestCarpenter();
        Furniture table = new Table();
        Furniture chair = new Chair();
        System.out.println(bestCarpenter.isCarpenterCanRepair(table));
        System.out.println(bestCarpenter.isCarpenterCanRepair(chair));
    }
}
