
package HW3.FirstPart;

public class RealizationOfDayOfWeek {
    public static void main(String[] args) {
        String[] namesOfDaysArray = new String[]{"Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday", "Sunday"};
        DayOfWeek[] daysOfWeekArray = new DayOfWeek[7];
        for (byte i = 0; i < daysOfWeekArray.length; i++) {
            daysOfWeekArray[i] = new DayOfWeek();
            daysOfWeekArray[i].numberOfDay = (byte) (i + 1);
            daysOfWeekArray[i].nameOfDay = namesOfDaysArray[i];
        }
        for (int i = 0; i < daysOfWeekArray.length; i++) {
            System.out.println(daysOfWeekArray[i].numberOfDay + " " + daysOfWeekArray[i].nameOfDay);
        }
    }
}
