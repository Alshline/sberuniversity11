
/**
 * Необходимо реализовать класс AmazingString, который хранит внутри себя
 * строку как массив char и предоставляет следующий функционал:
 * Конструкторы:
 * ● Создание AmazingString, принимая на вход массив char
 * ● Создание AmazingString, принимая на вход String
 * Публичные методы (названия методов, входные и выходные параметры
 * продумать самостоятельно):
 * ● Вернуть i-ый символ строки
 * ● Вернуть длину строки
 * ● Вывести строку на экран
 * ● Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается массив char). Вернуть true, если найдена и false иначе
 * ● Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается String). Вернуть true, если найдена и false иначе
 * ● Удалить из строки AmazingString ведущие пробельные символы, если
 * они есть
 * ● Развернуть строку (первый символ должен стать последним, а
 * последний первым и т.д.)
 */

package HW3.FirstPart;

public class AmazingString {
    private char[] amazingStringCharArray;

    public AmazingString(char[] charArray) {
        this.amazingStringCharArray = charArray;
    }

    public AmazingString(String string) {
        this.amazingStringCharArray = string.toCharArray();
    }

    public char getChar(int i) {
        return amazingStringCharArray[i];
    }

    public int getCharLength() {
        return amazingStringCharArray.length;
    }

    public void printCharArray() {
        System.out.println(String.valueOf(amazingStringCharArray));
    }

    public boolean checkSubstring(char[] checkedCharArray) {
        String bufferString = String.valueOf(amazingStringCharArray);
        String gotString = String.valueOf(checkedCharArray);
        return bufferString.contains(gotString);
        //return Arrays.asList(amazingStringCharArray).contains(checkedCharArray);
    }

    public boolean checkSubstring(String checkedString) {
        String bufferString = String.valueOf(amazingStringCharArray);
        return bufferString.contains(checkedString);
        // return Arrays.asList(amazingStringCharArray).contains(checkedString);
    }

    public void deleteWhiteSpaces() {
        String bufferString = new String(amazingStringCharArray);
        String trimmedString = bufferString.trim();
        this.amazingStringCharArray = trimmedString.toCharArray();
    }

    public void reverseCharArray() {
        char[] reversedCharArray = new char[amazingStringCharArray.length];
        int maxIndex = reversedCharArray.length - 1;
        for (int i = 0; i < amazingStringCharArray.length; i++) {
            reversedCharArray[maxIndex - i] = amazingStringCharArray[i];
        }
        amazingStringCharArray = reversedCharArray;
    }

    @Override
    public String toString() {
        return String.valueOf(amazingStringCharArray);
    }
}
