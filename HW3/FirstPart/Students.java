
/**
 * Необходимо реализовать класс Student.
 * У класса должны быть следующие приватные поля:
 * ● String name — имя студента
 * ● String surname — фамилия студента
 * ● int[10] grades — последние 10 оценок студента
 * И следующие публичные методы:
 * ● геттер/сеттер для name
 * ● геттер/сеттер для surname
 * ● геттер/сеттер для grades
 * ● метод, добавляющий новую оценку в grades. Самая первая оценка
 * должна быть удалена, новая должна сохраниться в конце массива
 * (пример: 2 5 4, добавляется 3, становится 5 4 3)
 * ● метод, возвращающий средний балл студента (рассчитывается как
 * среднее арифметическое от всех оценок в массиве grades)
 */

package HW3.FirstPart;

public class Students {
    private String name;
    private String surName;
    private int[] grades = new int[10];

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        if (grades == null) {
            System.out.println("Неверный ввод");
        } else if (grades.length > 10) {
            System.out.println("Неверная длина");
        } else {
            this.grades = grades;
        }
    }

    public void addNewGrade(int grade) {
        // Не хотелось делать через "любимый" цикл for(), но ввиду ограниченного размера содержащегося массива - так быстрее и проще.
        int[] bufferArray = new int[10];
        for (int i = 0; i < 9; i++) {
            bufferArray[i] = this.grades[i + 1];
        }
        bufferArray[9] = grade;
        this.grades = bufferArray;
    }

    //Не совсем ясно, требуется ли округление, потому выбор пал на тип double
    public double getAverageGrade() {
        double sumOfGrades = 0;
        for (int iterator : this.grades) {
            sumOfGrades += iterator;
        }
        return sumOfGrades / grades.length;
    }
}
