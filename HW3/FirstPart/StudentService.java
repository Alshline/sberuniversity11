
/**
 * Необходимо реализовать класс StudentService.
 * У класса должны быть реализованы следующие публичные методы:
 * ● bestStudent() — принимает массив студентов (класс Student из
 * предыдущего задания), возвращает лучшего студента (т.е. который
 * имеет самый высокий средний балл). Если таких несколько — вывести
 * любого.
 * ● sortBySurname() — принимает массив студентов (класс Student из
 * предыдущего задания), сортирует массив по фамилии в алфавитном
 * порядке и возвращает его.
 */

package HW3.FirstPart;

public class StudentService {
    private StudentService() {
    }

    public static Students bestStudent(Students[] studentsArray) {
        if (studentsArray.length == 1) {
            return studentsArray[0];
        } else if (studentsArray == null) {
            System.out.println("Неверные данные");
            return null;
        } else {
            Students bestStudent = studentsArray[0];
            for (int i = 1; i < studentsArray.length; i++) {
                if (studentsArray[i].getAverageGrade() > bestStudent.getAverageGrade()) {
                    bestStudent = studentsArray[i];
                }
            }
            return bestStudent;
        }
    }

    public static Students[] sortByName(Students[] studentsArray) {
        Students[] sortedStudentsArray = studentsArray.clone();
        for (int i = studentsArray.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                String firstString = sortedStudentsArray[j].getName();
                String secondString = sortedStudentsArray[j + 1].getName();
                if (firstString.compareTo(secondString) > 0) {
                    Students bufferStudent = sortedStudentsArray[j];
                    sortedStudentsArray[j] = sortedStudentsArray[j + 1];
                    sortedStudentsArray[j + 1] = bufferStudent;
                }
            }
        }
        return sortedStudentsArray;
    }
}
