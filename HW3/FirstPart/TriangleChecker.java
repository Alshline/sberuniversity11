
/**
 * Реализовать класс TriangleChecker, статический метод которого принимает три
 * длины сторон треугольника и возвращает true, если возможно составить из них
 * треугольник, иначе false. Входные длины сторон треугольника — числа типа
 * double. Придумать и написать в методе main несколько тестов для проверки
 * работоспособности класса (минимум один тест на результат true и один на
 * результат false)
 */

package HW3.FirstPart;

public class TriangleChecker {
    private TriangleChecker() {

    }

    public static boolean checkTriangle(double firstLine, double secondLine, double thirdLine) {
        if (((firstLine + secondLine) > thirdLine) && ((secondLine + thirdLine) > firstLine) && ((firstLine + thirdLine) > secondLine)) {
            return true;
        } else {
            return false;
        }
    }
}
