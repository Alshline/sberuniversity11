
/**
 * Реализовать класс “банкомат” Atm.
 * Класс должен:
 * ● Содержать конструктор, позволяющий задать курс валют перевода
 * долларов в рубли и курс валют перевода рублей в доллары (можно
 * выбрать и задать любые положительные значения)
 * ● Содержать два публичных метода, которые позволяют переводить
 * переданную сумму рублей в доллары и долларов в рубли
 * ● Хранить приватную переменную счетчик — количество созданных
 * инстансов класса Atm и публичный метод, возвращающий этот счетчик
 * (подсказка: реализуется через static)
 */

package HW3.FirstPart;

public class Atm {
    private static int count = 0;
    private double dollarToRoubleCourse;
    private double roubleToDollarCourse;

    public Atm(double dollarToRoubleCourse, double roubleToDollarCourse) {
        this.dollarToRoubleCourse = dollarToRoubleCourse;
        this.roubleToDollarCourse = roubleToDollarCourse;
        this.count++;
    }

    public int getCount() {
        return count;
    }

    public double getDollarsFromRoubles(double roubles) {
        return roubles * dollarToRoubleCourse;
    }

    public double getRoublesFromDollars(double dollars) {
        return dollars * roubleToDollarCourse;
    }
}
