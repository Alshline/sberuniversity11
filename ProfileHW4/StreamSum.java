/**
 * Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
 * экран.
 */

package ProfileHW4;

import java.util.stream.IntStream;

public class StreamSum {
    public static void main(String[] args) {
        System.out.println(getSum(2, 101));
        System.out.println(defaultGetSum());
    }

    public static int getSum(Integer leftRange, Integer rightRange) {
        Integer result = IntStream
                .range(leftRange, rightRange)
                .filter(value -> value % 2 == 0)
                .sum();
        return result;
    }

    //На случай если метод выше не подходит под задание (надо четко диапазон от 1 до 100)
    public static int defaultGetSum() {
        Integer result = IntStream
                .range(0, 101)
                .filter(value -> value % 2 == 0)
                .sum();
        return result;
    }
}
