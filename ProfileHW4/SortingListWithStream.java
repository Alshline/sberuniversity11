/**
 * На вход подается список вещественных чисел. Необходимо отсортировать их по
 * убыванию.
 */

package ProfileHW4;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SortingListWithStream {
    public static void main(String[] args) {
        List<Double> doubleList = new ArrayList<>();
        doubleList.add(1.5);
        doubleList.add(29.0);
        doubleList.add(21.2);
        doubleList.add(456.3);
        doubleList.add(18.5);
        doubleList.add(15.6);
        System.out.println(getSortedList(doubleList));
    }

    public static List<Double> getSortedList(List<Double> doubleList) {
        return doubleList
                .stream()
                .sorted((o1, o2) -> o2.compareTo(o1))
                .collect(Collectors.toList());
    }
}
