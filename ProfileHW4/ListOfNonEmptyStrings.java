/**
 * На вход подается список строк. Необходимо вывести количество непустых строк в
 * списке.
 * Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */

package ProfileHW4;

import java.util.List;
import java.util.stream.Collectors;

public class ListOfNonEmptyStrings {
    public static void main(String[] args) {
        List<String> stringList = List.of("abc", "", "", "def", "qqq");
        System.out.println(sumOfNonEmptyStrings(stringList));
    }

    public static long sumOfNonEmptyStrings(List<String> stringList) {
        return stringList.stream()
                .filter(string -> string.trim().length() != 0)
                .collect(Collectors.toList())
                .stream().count();
    }
}
