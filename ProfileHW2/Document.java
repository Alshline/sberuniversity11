package ProfileHW2;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    //Only for Tests
    public Document (int id, String name, int pageCount){
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    //Only for tests
    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }
}
