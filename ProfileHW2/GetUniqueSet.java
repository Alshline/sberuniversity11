/**
 * Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
 * набор уникальных элементов этого массива. Решить используя коллекции.
 */

package ProfileHW2;

import java.util.*;

public class GetUniqueSet<T> {
    public static <T> HashSet<T> getUniqueElements(List<T> arrayList){
        HashSet<T> set = new HashSet<>(arrayList);
        return set;
    }

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(12);
        arrayList.add(11);
        arrayList.add(13);
        arrayList.add(11);
        arrayList.add(112);
        arrayList.add(12);
        arrayList.add(12);

        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("1");
        arrayList1.add("12");
        arrayList1.add("1");
        arrayList1.add("13");
        arrayList1.add("1");
        arrayList1.add("14");
        arrayList1.add("14");

        HashSet hashSet = getUniqueElements(arrayList);
        HashSet hashSet1 = getUniqueElements(arrayList1);
        System.out.println(hashSet);
        System.out.println(hashSet1);
    }
}
