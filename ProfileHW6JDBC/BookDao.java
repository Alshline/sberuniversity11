package ProfileHW6JDBC;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDao implements DaoInterface<Book> {

    @Override
    public Book get(Integer id) {
        try {
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM books WHERE id=" + id);
            if (resultSet.next()) {
                return getBookFromResultSet(resultSet);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public Book getByName(String bookName) {
        try {
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM books WHERE bookname='" + bookName + "'");
            if (resultSet.next()) {
                return getBookFromResultSet(resultSet);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public List getAll() {
        try {
            List<Book> bookList = new ArrayList<>();
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM books");
            while (resultSet.next()) {
                bookList.add(getBookFromResultSet(resultSet));
            }
            return bookList;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public void add(Book book) {
        Statement statement = null;
        String formString = "insert into books (bookName) values('%s')";
        try {
            statement = ManagerClass.getConnection().createStatement();
            statement.executeUpdate(String.format(formString, book.getBookName()));
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void update(Book book) {
        Statement statement = null;
        String formString = "UPDATE books SET bookName='%s' WHERE id='%d'";
        try {
            statement = ManagerClass.getConnection().createStatement();
            statement.executeUpdate(String.format(formString, book.getBookName(), book.getId()));
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM books WHERE id=" + id);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    private Book getBookFromResultSet(ResultSet resultSet) throws SQLException {
        Book book = new Book();
        book.setId(resultSet.getInt("id"));
        book.setBookName(resultSet.getString("bookName"));
        return book;
    }
}
