package ProfileHW5;

public interface DBManagerInterface {

    //По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
    void getById(int id);

    //Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
    void getByClientPerMonth(int clientId);

    //Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
    void getMaxByFlowers ();

    //Вывести общую выручку (сумму золотых монет по всем заказам) за все время
    void getRevenue();
}
