package HW1.SecondPart;

import java.util.Scanner;

/**
 * "А логарифмическое?" - не унималась дочь.
 * Напишите программу, которая проверяет, что log(e^n) == n для любого
 * вещественного n.
 * Параметры
 * Scanner input = new Scanner(System.in);
 * double n = input.nextDouble();
 *
 * @author Кашин Андрей
 */

public class Task10 {

    public static void main(String[] args) {

        final double epsilon = 0.000001;

        System.out.println("Введите n");
        Scanner input = new Scanner(System.in);
        double n = input.nextDouble();

        double logarithm = Math.log( Math.pow(Math.E , n) );

        /*
        В данной ситуации при применении n = 987651 программа выдавала false, если использовать Double.compare.
         */
        boolean result = Math.abs(logarithm - n) < epsilon;
        System.out.println(result);
    }
}
