package HW1.ThirdPart;

/**
 * Напечатать таблицу умножения от 1 до 9.
 */

public class Task1 {

    public static void main(String[] args) {

        int i;  // множимое
        int j;  // множитель

        for (i = 1;i < 10;i++){

            for (j=1; j < 10; j++){

                int result = i*j;
                System.out.println(i + " x " + j + " = " + result);
            }
        }
    }
}
