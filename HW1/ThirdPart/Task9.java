package HW1.ThirdPart;

import java.util.Scanner;

/**
 * На вход подается числа n - длина последовательность. Далее на вход подается
 * возрастающая последовательность из n целых чисел, которая может
 * начинаться с отрицательного числа. Определить, какое количество
 * отрицательных чисел записано в начале последовательности. Помимо этого
 * нужно прекратить выполнение цикла при получении первого неотрицательного
 * числа на вход
 */

public class Task9 {

    public static void main(String[] args) {

        Scanner inputHowMany = new Scanner(System.in);
        System.out.println("Введите количество чисел");

        int n = inputHowMany.nextInt();

        Scanner inputValues = new Scanner(System.in);
        System.out.println("Введите последовательность чисел");

        int a;
        int count = 0;

        while ( (a = inputValues.nextInt()) < 0 && count <= n){
            count++;
        }

        System.out.println(count);
    }
}
