package HW1.ThirdPart;

import java.util.Scanner;

/**
 * Даны положительные числа m и n. Найти остаток от деления m на n, не
 * выполняя операцию взятия остатка.
 */

public class Task5 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Введите делимое число");
        int firstNumber = input.nextInt();

        System.out.println("Введите делитель");
        int secondNumber = input.nextInt();

        // first method
        int tail = firstNumber - ((int) firstNumber/secondNumber)*secondNumber;
        System.out.println(tail);

        // second method (K.I.S.S.)
        int buffer = 0;
        for ( ; ; ){
            if (buffer < firstNumber){
                buffer += secondNumber;
            } else {
                break;
            }
        }

        int secondTail = firstNumber - buffer + secondNumber;
        System.out.println(secondTail);
    }
}
