package HW1.ThirdPart;

import java.util.Scanner;

/**
 * Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N. На N +
 * 1 строке у “ёлочки” должен быть отображен ствол из символа |
 * На вход подается число N не меньше 3.
 */

public class Task10 {

    public static void main(String[] args) {

        Scanner inputHeight = new Scanner(System.in);
        System.out.println("Введите высоту");

        int height = inputHeight.nextInt();

        int indent;
        int countOfStars = 1;

        for (int i = 0; i < height; i++){
            indent = height - 1 - i;
            for (int l = 1; l <= indent; l++){
                System.out.print(" ");
            }
            for (int m = 1; m <= countOfStars; m++){
                System.out.print("*");
            }
            for (int n = 1; n <= indent; n++){
                System.out.print(" ");
            }
            countOfStars += 2;
            System.out.println();
        }

        for (int k = 1; k < height; k++){
            System.out.print(" ");
        }
        System.out.println("|");

    }
}
