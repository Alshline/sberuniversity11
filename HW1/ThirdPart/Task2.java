package HW1.ThirdPart;

import java.util.Scanner;

/**
 * На вход подается два положительных числа m и n (m < 10, n < 10, m < n). Найти
 * сумму чисел между m и n включительно.
 */

public class Task2 {

    public static void main(String[] args) {

        System.out.println("Введите два положительных числа");
        Scanner input = new Scanner(System.in);
        int firstNumber = input.nextInt();
        int secondNumber = input.nextInt();

        int sum = 0;

        for (int i = firstNumber; i <= secondNumber; i++){
            sum += i;
        }

        System.out.println(sum);
    }
}
