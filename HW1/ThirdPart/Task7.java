package HW1.ThirdPart;

import java.util.Scanner;

/**
 *Дана строка s. Вычислить количество символов в ней, не считая пробелов
 * (необходимо использовать цикл).
 */

public class Task7 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Введите строку");
        String s = input.nextLine();

        char end = '\0'; // возможно стоит использовать неиспользуемый символ или просто .lenght()

        String trimS = s.trim();
        StringBuffer bufferedString = new StringBuffer(trimS);
        bufferedString.append(end);

        boolean flag = true;
        int index = 0;

        while (flag){
            if (bufferedString.charAt(index) == end){
                break;
            } else index++;
        }

        System.out.println(index);
    }
}
