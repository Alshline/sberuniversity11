package HW1.ThirdPart;

import java.util.Scanner;

/**
 * На вход подается:
 * ○ целое число n,
 * ○ целое число p
 * ○ целые числа a1, a2 , … an
 * Необходимо вычислить сумму всех чисел a1, a2, a3… an которые больше p.
 */

public class Task8 {

    public static void main(String[] args) {


        Scanner input = new Scanner(System.in);

        System.out.println("Введите числа n, p, последовательность чисел а");
        int n = input.nextInt();
        int p = input.nextInt();

        int a;
        int sum = 0;

        Scanner inputNumbers = new Scanner(System.in);

        for (int i = 1; i <= n; i++){
            a = inputNumbers.nextInt();
            if (a > p){
                sum += a;
            }
        }

        System.out.println(n);
        System.out.println(p);
        System.out.println(sum);
    }
}
